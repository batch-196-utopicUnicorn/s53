import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard'
import {useState, useEffect} from 'react';

export default function Courses(){
	//console.log(coursesData)
	//console.log(coursesData[0])

/*	const courses = coursesData.map(kors => {
		console.log(kors)
		return(
			<CourseCard key={kors.id} courseProp = {kors}/>

		)

	})*/

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/courses')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course => {
				return(
		<CourseCard key={course._id} courseProp = {course}/>
					)
			}))
		})

	}, [])

	return(

		<>

			<h1>Available Courses:</h1>
			{courses}
			
		</>
	)
}