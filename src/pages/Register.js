import {useState} from 'react';
import {Navigate} from 'react-router-dom'
import {useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Register(){
	const {user} = useContext(UserContext)
	const history = useNavigate()

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('')
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setActive] = useState(false)
	
	console.log(email);
	

	function registerUser(e){
		//prevents page redirection via form submission
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmailExists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data) {
				Swal.fire({
					title: "Duplicate email found",
					icon: "info",
					text: "The email that you're trying to register already exist"
				})
			}else{

				fetch('http://localhost:4000/users', {
					method: "POST",
					headers: {
					'Content-Type': 'application/json'
					},
					body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					mobileNo: mobileNo,
					email:email,
					password: password
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data.email){
					Swal.fire({
					title: "Registration Success",
					icon: "success",
					text: "You have successfully registered"
				})
					history('/login')
					
				}else {
					Swal.fire({
					title: "Registration failed",
					icon: "success",
					text: "Something went wrong, please try again"
				})
				}
			})
				
			}
		})

		//clear input fields
		setFirstName('');
		setLastName('');
		setMobileNo('')
		setEmail('');
		setPassword('');
		

		//alert('Thank you for registering');
	}

	useEffect(() => {
		if(email !== '' && firstName !== '' && lastName !== '' && password !== '' && mobileNo !== '' && mobileNo.length === 11){
			setActive(true);
		} else{
			setActive(false)
		}
	}, [email, firstName, lastName, mobileNo, password])


	return(
		
		(user.id !== null) ?

			<Navigate to ='/courses'/>
		:
		<>
		<h1>Register Here:</h1>
		<Form onSubmit={e => registerUser(e)}>
		<Form.Group className="mt-4" controlId="userFirstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
				type="text"
				placeholder="Enter your first name here"
				required
				value = {firstName}
				onChange = {e => setFirstName(e.target.value)}
				/>
				
			</Form.Group>
			<Form.Group className="mt-4" controlId="userLastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
				type="text"
				placeholder="Enter your last name here"
				required
				value = {lastName}
				onChange = {e => setLastName(e.target.value)}
				/>
				
			</Form.Group>
			
			<Form.Group className="mt-4" controlId="userMobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
				type="text"
				placeholder="Enter your 11-digit number here"
				required
				value = {mobileNo}
				onChange = {e => setMobileNo(e.target.value)}
				/>
				
			</Form.Group>
			<Form.Group className="mt-4" controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
				type="email"
				placeholder="Enter your email here"
				required
				value = {email}
				onChange = {e => setEmail(e.target.value)}
				/>
			</Form.Group>
			<Form.Group className="mt-4" controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
				type="password"
				placeholder="Enter your password here"
				required
				value = {password}
				onChange = {e => setPassword(e.target.value)}
				/>
				</Form.Group>
			

		{ isActive ?
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">
				Register
			</Button>
			:
			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>
				Register
			</Button>
		}

			</Form>
		</>
	)
}

